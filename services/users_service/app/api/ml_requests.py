import requests as r

ML_SERVICE_ADDRESS = "http://ml_service:80"
CLOSEST_PATH = '/get_closest'


def send_ml_request(original, others):
    response = r.get(ML_SERVICE_ADDRESS + CLOSEST_PATH, json={
        'original_descriptions': original,
        'others': others
    })
    print(response.text)
    if response.status_code != 200:
        return {"status": "failed",
                "info": response.text}
    else:
        return response.json()