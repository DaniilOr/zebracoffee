import datetime

from pydantic import BaseModel
from typing import List, Dict, Any, Optional


class ItemBase(BaseModel):
    pass


class Item(ItemBase):
    item_name: str
    food_type: str
    price: int
    true_cost: int
    image: str
    description: str

    class Config:
        orm_mode = True


class StoreBase(BaseModel):
    pass


class Store(StoreBase):
    id: Optional[int]
    address: str
    lat: float
    lon: float
    phone: str
    constant_cost: int

    class Config:
        orm_mode = True


class OrderBase(BaseModel):
    pass


class Order(OrderBase):
    id: int
    order_date: datetime.date
    store_id: int
    user_id: int
    items: List[int]
    total_cost: int
    total_true_cost: int
    status: str = "pending"
    used_discount: bool


class CreatingOrder(BaseModel):
    user_id: str
    items: Dict[int, int]
    location_id: int


class IncomingDiscount(BaseModel):
    user_id: str
    amount: int


class UpdStatus(BaseModel):
    status: str
    order_id: int


class Feedback(BaseModel):
    order_id: int
    feedback: str
    score: int


class HoursBase(BaseModel):
    pass


class WorkingHours(HoursBase):
    date: datetime.datetime = datetime.datetime.now().date()
    start: datetime.datetime = datetime.datetime.now()
    end: Optional[datetime.datetime]

    cachier_id: str
    store_id: int


class StatsReport(BaseModel):
    working_hours: Any
    items_frequences: Any
    user_visits: Any
    user_spendings: Any
    marginality: Any