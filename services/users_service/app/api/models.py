import datetime

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float, DateTime, ARRAY
from sqlalchemy.orm import relationship

from .database import Base


class Store(Base):
    __tablename__ = "store"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    address = Column(String, index=True)
    lat = Column(Float, index=True)
    lon = Column(Float, index=True)
    phone = Column(String, index=True)
    constant_cost = Column(Integer, index=True)

    orders = relationship("Orders", back_populates="store")
    workinghours = relationship("WorkingHours", back_populates="store")


class Orders(Base):
    __tablename__ = "orders"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    order_date = Column(DateTime, index=True, default=datetime.datetime.now())
    store_id = Column(Integer, ForeignKey('store.id'))
    user_id = Column(String, index=True)
    items = Column(ARRAY(Integer), index=True)
    total_cost = Column(Integer, index=True)
    total_true_cost = Column(Integer, index=True)
    status = Column(String, index=True, default="pending")
    used_discount = Column(Boolean, index=True)

    feedback = Column(String, index=True, default="")
    score = Column(Integer, index=True, default=-1)

    store = relationship("Store", back_populates="orders")


class Items(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    food_type = Column(String, index=True)
    item_name = Column(String, index=True)
    price = Column(Integer, index=True)
    true_cost = Column(Integer, index=True)
    image = Column(String, index=True)
    description = Column(String, index=True)


class Discount(Base):
    __tablename__ = "discounts"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    user_id = Column(String, index=True)
    amount = Column(Integer, index=True)


class WorkingHours(Base):
    __tablename__ = "hours"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    date = Column(DateTime, index=True, default=datetime.datetime.now().date())
    start = Column(DateTime, index=True, default=datetime.datetime.now())
    end = Column(DateTime, index=True, nullable=True)

    cachier_id = Column(String, index=True)
    store_id = Column(Integer, ForeignKey('store.id'))

    store = relationship("Store", back_populates="workinghours")
