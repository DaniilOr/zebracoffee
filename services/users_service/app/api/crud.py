import datetime

from sqlalchemy.orm import Session
from fastapi import HTTPException

from . import models, schemas


def get_stores(db: Session):
    return db.query(models.Store).all()


def get_user_orders(db: Session, user_id):
    return db.query(models.Orders).filter(models.Orders.user_id == user_id).all()


def add_location(db: Session, store: schemas.Store):
    db_store = models.Store(
                            address=store.address,
                            lat=store.lat,
                            lon=store.lon,
                            phone=store.phone,
                            constant_cost=store.constant_cost)
    db.add(db_store)
    db.commit()
    db.refresh(db_store)
    return db_store


def add_order(db: Session, order: models.Orders):
    db.add(order)
    db.commit()
    return order


# def add_menu_items(db: Session, item: schemas.Item):
#     item = models.Items(food_type=item.food_type, item_name=item.item_name,
#                         price=item.price, true_cost=item.true_cost,
#                         image=item.image, description=item.description)
#     db.add(item)
#     db.commit()
#     return item


def get_menu(db: Session):
    return db.query(models.Items).all()


def get_item_byid(db: Session, item_id):
    return db.query(models.Items).filter(models.Items.id == item_id).first()


def update_status(db: Session, order_id: int, status: str):
    order = db.query(models.Orders).filter(models.Orders.id == order_id).first()
    if not order:
        raise HTTPException(status_code=200, detail="Order not found")
    setattr(order, 'status', status)
    db.add(order)
    db.commit()
    db.refresh(order)
    return order


def get_discount(db: Session, user_id: str):
    return db.query(models.Discount).filter(models.Discount.user_id == user_id).first()


def add_discount(db: Session, user_id: str, amount: int):
    discount = db.query(models.Discount).filter(models.Discount.user_id == user_id).first()
    if discount is None:
        discount = models.Discount(user_id=user_id,
                                   amount=amount)
    else:
        discount.amount = amount
    db.add(discount)
    db.commit()
    db.refresh(discount)
    return discount


def add_menu_item(db: Session, item: schemas.Item):
    item = models.Items(
                        food_type=item.food_type,
                        price=item.price, true_cost=item.true_cost,
                        image=item.image, description=item.description,
                        item_name=item.item_name
                        )
    db.add(item)
    db.commit()
    db.refresh(item)
    return item


def delete_menu_item(db: Session, menu_item_id: int):
    item = db.query(models.Items).get(menu_item_id)
    if item is None:
        raise HTTPException(status_code=200, detail="Item not found")
    db.delete(item)
    db.commit()
    return item


def update_menu_item(db: Session, menu_item_id: int,
                     item: schemas.Item):
    old_item = db.query(models.Items).get(menu_item_id)
    if old_item is None:
        raise HTTPException(status_code=200, detail="Item not found")
    item = item.dict(exclude_unset=True)
    for key, value in item.items():
        setattr(old_item, key, value)
    db.add(old_item)
    db.commit()
    db.refresh(old_item)
    return old_item


def get_menu_item(db: Session, menu_item_id: int):
    item = db.query(models.Items).get(menu_item_id)
    if item is None:
        raise HTTPException(status_code=200, detail="Item not found")
    return item


def delete_store(db: Session, store_id: int):
    store = db.query(models.Store).get(store_id)
    if store is None:
        raise HTTPException(status_code=200, detail="Store not found")
    db.delete(store)
    db.commit()
    return store


def update_store(db: Session, store_id: int,
                     store: schemas.Store):
    old_store = db.query(models.Store).get(store_id)
    if old_store is None:
        raise HTTPException(status_code=200, detail="Store not found")
    store = store.dict(exclude_unset=True)
    for key, value in store.items():
        setattr(old_store, key, value)
    db.add(old_store)
    db.commit()
    db.refresh(old_store)
    return old_store


def get_store(db: Session, store_id: int):
    store = db.query(models.Store).get(store_id)
    if store is None:
        raise HTTPException(status_code=200, detail="Store not found")
    return store


def add_feedback(db: Session, feedback: schemas.Feedback):
    order = db.query(models.Orders).get(feedback.order_id)
    if order is None:
        raise HTTPException(status_code=200, detail="Order not found")
    order.feedback = feedback.feedback
    order.score = feedback.score


def add_working_hours(db: Session, working_hours: schemas.WorkingHours):
    working_hours = models.WorkingHours(
        cachier_id=working_hours.cachier_id,
        store_id=working_hours.store_id
    )
    db.add(working_hours)
    db.commit()
    db.refresh(working_hours)
    return working_hours


def finish_working(db: Session, working_hours_id: int):
    working_hours = db.query(models.WorkingHours).get(working_hours_id)
    if working_hours is None:
        raise HTTPException(status_code=200, detail="No match")
    working_hours.end = datetime.datetime.now()
    db.add(working_hours)
    db.commit()
    db.refresh(working_hours)
    return working_hours


def get_all_workinghours(db: Session):
    working_hours = db.query(models.WorkingHours).all()
    return working_hours


def get_all_stores(db: Session):
    stores = db.query(models.Store).all()
    return stores


def get_all_orders(db: Session):
    orders = db.query(models.Orders).all()
    return orders


def get_exclusive_orders(db: Session, user_id: str):
    orders = db.query(models.Orders).filter(models.Orders.user_id != user_id).all()
    return orders


def update_discount(db: Session, user_id: str, amount: int):
    discount = db.query(models.Discount).filter(models.Discount.user_id == user_id).first()
    if discount is None:
        raise HTTPException(status_code=200, detail="No discount for this user")
    discount.amount = amount
    db.add(discount)
    db.commit()
    db.refresh(discount)
    return discount
