from ..api import models
import typing as t
import pandas as pd


def dict2list(orders):
    result = []
    for item, amount in orders.items():
        result += [item] * amount
    return result


def calculate_total_cost(items: t.List[models.Items]):
    cost = 0
    true_cost = 0
    for item in items:
        print(item)
        cost += item.price
        true_cost += item.true_cost
    return cost, true_cost


def pandify(lst):
    new_lst = [item.__dict__ for item in lst]
    return pd.DataFrame(new_lst)


if __name__ == "__main__":
    print(dict2list({1: 1, 2:2, 3:3, 4:1, 5:7}))