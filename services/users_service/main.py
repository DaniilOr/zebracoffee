import pandas as pd
from typing import List, Dict
from random import random, randint

from fastapi import Depends, FastAPI
from sqlalchemy.orm import Session
from fastapi import HTTPException


from app.api import crud, models, schemas, ml_requests
from app.api.database import SessionLocal, engine
from app.utils import simpleutils

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

"""

            User part

"""


@app.get("/locations", response_model=List[schemas.Store])
async def get_locations(db: Session = Depends(get_db)):
    locations = crud.get_stores(db)
    return locations



@app.get("/orders/{user_id}")
async def get_user_orders(user_id: str, db: Session = Depends(get_db), response_model=List[schemas.Order]):
    return crud.get_user_orders(db, user_id)


@app.post('/create_order')
async def create_order(order: schemas.CreatingOrder, db: Session = Depends(get_db),
                       responce_code=201):
    items_ids = simpleutils.dict2list(order.items)
    items = list(map(crud.get_item_byid, [db]*len(items_ids), items_ids))
    cost, true_cost = simpleutils.calculate_total_cost(items)

    # get discount
    discount = crud.get_discount(db, order.user_id)
    if discount is None:
        discount = 0
    else:
        discount = discount.amount

    # adjust cost
    cost = int(cost - discount/100 * cost)

    # pack order
    order = models.Orders(store_id=order.location_id,
                          user_id=order.user_id,
                          items=items_ids,
                          total_cost=cost,
                          total_true_cost=true_cost,
                          used_discount=discount > 0)
    crud.add_order(db, order)
    return order


@app.get("/populate")
async def populate(db: Session = Depends(get_db), status_code=201):
    locations = [
        schemas.Store(id=i, address=f'address_{i}', lat=random(),
                      lon=random(), phone=str(i), constant_cost=randint(10, 100))
        for i in range(5)
    ]
    for loc in locations:
        crud.add_location(db, loc)
    return {'status': 'ok'}


@app.get("/menu")
async def get_menu(db:Session = Depends(get_db), response_model=List[schemas.Item]):
    menu = crud.get_menu(db)
    return menu


# @app.post("/add_menu_item")
# async def get_menu(item: schemas.Item, db:Session = Depends(get_db),):
#     menu = crud.add_menu_items(db, item)
#     return menu


@app.post("/update_order_status")
async def get_menu(upd: schemas.UpdStatus,  db:Session = Depends(get_db)):
    new_order = crud.update_status(db, upd.order_id, upd.status)
    return new_order


@app.post("/add_feedback")
async def get_menu(feedback: schemas.Feedback,  db:Session = Depends(get_db)):
    new_order = crud.update_status(db, upd.order_id, upd.status)
    return new_order


"""

            Admin part

"""


@app.post('/create_menu_item')
async def create_menu_item(item: schemas.Item, db: Session = Depends(get_db),
                           response_model=models.Items):
    item = crud.add_menu_item(db, item)
    return item


@app.delete('/delete_menu_item/{menu_item_id}')
async def delete_menu_item(menu_item_id: int, db: Session = Depends(get_db),
                           response_model=models.Items):
    res = crud.delete_menu_item(db, menu_item_id)
    return res


@app.put('/update_menu_item/{menu_item_id}')
async def update_menu_item(menu_item_id: int,
                           item: schemas.Item,
                           db: Session = Depends(get_db),
                           response_model=models.Items):
    res = crud.update_menu_item(db, menu_item_id, item)
    return res


@app.get('/get_menu_item/{menu_item_id}')
async def get_menu_item(menu_item_id: int,
                           db: Session = Depends(get_db),
                           response_model=models.Items):
    res = crud.get_menu_item(db, menu_item_id)
    return res


@app.post('/create_store')
async def create_store(store: schemas.Store, db: Session = Depends(get_db),
                           response_model=models.Items):
    item = crud.add_location(db, store)
    return item


@app.delete('/delete_store/{store_id}')
async def delete_store(store_id: int, db: Session = Depends(get_db),
                           response_model=models.Items):
    res = crud.delete_store(db, store_id)
    return res


@app.put('/update_store/{store_id}')
async def update_store(store_id: int,
                           store: schemas.Store,
                           db: Session = Depends(get_db),
                           response_model=models.Items):
    res = crud.update_store(db, store_id, store)
    return res


@app.get('/get_store/{store_id}')
async def get_store(store_id: int,
                           db: Session = Depends(get_db),
                           response_model=models.Items):
    res = crud.get_store(db, store_id)
    return res


@app.post('/add_discount')
async def add_discount(discount: schemas.IncomingDiscount, db:Session = Depends(get_db)):
    discount = crud.add_discount(db, discount.user_id, discount.amount)
    return discount


@app.get('/get_discount/{user_id}')
async def get_discount(user_id: str, db:Session = Depends(get_db),
                       response_model=models.Discount):
    discount = crud.get_discount(db, user_id)
    return discount



"""

            Cachier part

"""


@app.post('/start_working')
async def start_cachiers_work(working_hour: schemas.WorkingHours, db:Session = Depends(get_db),
                       response_model=models.WorkingHours):
    wh = crud.add_working_hours(db, working_hour)
    return wh


@app.post('/stop_working/{working_hours_id}')
async def end_cachiers_work(working_hours_id: int, db:Session = Depends(get_db),
                       response_model=models.WorkingHours):
    wh = crud.finish_working(db, working_hours_id)
    return wh


@app.get('/stats')
async def get_status(db:Session = Depends(get_db),
                       response_model=schemas.StatsReport):
    all_working_hours = simpleutils.pandify(crud.get_all_workinghours(db))
    if len(all_working_hours) != 0:
        all_working_hours['worked_hours'] = all_working_hours['end'] - all_working_hours['start']
        all_working_hours = all_working_hours[['cachier_id', 'store_id', 'worked_hours']]


    all_stores = simpleutils.pandify(crud.get_all_stores(db))

    if len(all_stores) != 0:
        orders = simpleutils.pandify(crud.get_all_orders(db))
        orders['date'] = orders['order_date'].apply(lambda x: x.date())
        marginality_orders = orders.groupby(['store_id', 'date']).aggregate({"total_cost":'sum',
                                                         "total_true_cost":'sum'}).reset_index()
        marginality_orders = marginality_orders.merge(all_stores, left_on='store_id',
                                                      right_on='id')

        marginality_orders['earnings'] = marginality_orders['total_cost'] - marginality_orders['total_true_cost'] - \
                                         marginality_orders['constant_cost']

    users_df = orders.groupby('user_id').aggregate({"total_cost":'sum',
                                                     "total_true_cost":'sum'}).reset_index()

    user_visits = orders.groupby(['user_id', 'date'])['id'].count().reset_index().rename({'id':'visits'}, axis=1)


    items = pd.Series(orders['items'].sum()).value_counts()

    result = schemas.StatsReport(
        working_hours=all_working_hours.to_dict(orient='records'),
        items_frequences=items.to_dict(),
        user_visits=user_visits.to_dict(orient='records'),
        user_spendings=users_df.to_dict(orient='records'),
        marginality=marginality_orders.to_dict(orient='records'),

    )
    return result


"""

    Handling ML
    
"""


@app.get('/most_similar/{user_id}')
async def call_ml(user_id: str, db: Session = Depends(get_db),
                  response_model=List[schemas.Item]):
    user_orders = crud.get_user_orders(db, user_id)
    others = crud.get_exclusive_orders(db, user_id)
    user_descriptions = [crud.get_item_byid(db, item_id).description for order in user_orders for item_id in order.items]
    other_descriptions = {}
    for other in others:
        for item_id in other.items:
            if other.id in other_descriptions:
                other_descriptions[other.id].append(crud.get_item_byid(db, item_id).description)
            else:
                other_descriptions[other.id] = [crud.get_item_byid(db, item_id).description]
    others_list = list(other_descriptions.values())
    ml_response = ml_requests.send_ml_request(user_descriptions, others_list)
    if ml_response.get('match') is None:
        raise HTTPException(status_code=200, detail="ML failed to respond")
    needed_id = list(other_descriptions.keys())[ml_response]
    needed_orders = crud.get_user_orders(db, needed_id)
    result = []
    for order in needed_orders:
        result = result + order.items
    user_orders = crud.get_user_orders(db, user_id)
    user_items = []
    for order in user_orders:
        user_items = user_items + order.items
    result = [order for order in result if not user_items]
    return result


@app.get('/get_all_orders')
async def get_all_orders(db: Session = Depends(get_db),
                         response_model=List[schemas.Order]):
    orders = crud.get_all_orders(db)
    return orders