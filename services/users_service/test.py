import requests as r

ACTION = 'order'

if __name__=="__main__":
    if ACTION == 'order':
        r = r.post('http://localhost:8000/create_order', json={
            'user_id': 'b',
            'items': {
                1: 2,
            },
            'location_id': 7,
        })
    elif ACTION == 'menu':
        r = r.post('http://localhost:8000/create_menu_item', json={
            'id':1,
            'food_type': 'food',
            'item_name': 'potat',
            'price': 1650,
            'true_cost': 200,
            'image': 'http://icons.io',
            'description': 'good'
        })
    elif ACTION == 'discount':
        r = r.post('http://localhost:8000/add_discount', json={
            'user_id': 'aaa',
            'amount': 10,
        })
    elif ACTION == 'additem':
        r = r.post('http://localhost:8000/create_menu_item', json={
                'item_name': 'potato',
                'food_type': 'food',
                'price': 1200,
                'true_cost': 1000,
                'image': 'images.google.com',
                'description': 'not a cat',
        })
    elif ACTION == 'delitem':
        r = r.delete('http://localhost:8000/delete_menu_item/10')
    elif ACTION == 'upditem':
        r = r.put('http://localhost:8000/update_menu_item/1', json={
            'item_name': 'potato',
            'food_type': 'food',
            'price': 1200,
            'true_cost': 1000,
            'image': 'images.google.com',
            'description': 'not a cat',
        })
    elif ACTION == 'updstore':
        r = r.put('http://localhost:8000/update_store/1', json={
            'address': 'Улица пушкина',
            'lat': 36.6,
            'lon': 24,
            'phone': '8800553535',
            'constant_cost': 960,
        })
    elif ACTION == 'delstore':
        r = r.delete('http://localhost:8000/delete_store/0')
    elif ACTION == 'working':
        r = r.post('http://localhost:8000/start_working', json={
            'cachier_id': 'bbb',
            'store_id': 4,
        })
    elif ACTION == 'stopping':
        r = r.post('http://localhost:8000/stop_working/1')
    print(r.text)
