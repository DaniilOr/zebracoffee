from fastapi import Depends, FastAPI

from app.models import MlRequest
from app.ml import get_embeddings, find_closest


app = FastAPI()


@app.get('/get_closest')
async def get_similar(req: MlRequest):
    print(req.json())
    print("HUY" * 10)
    orig_embs, others_embs = get_embeddings(req.original_descriptions, req.others)
    closest = find_closest(orig_embs, others_embs)
    return {"match": closest}
