from pydantic import BaseModel
from typing import List


class MlRequest(BaseModel):
    original_descriptions: List[str]
    others: List[List[str]]