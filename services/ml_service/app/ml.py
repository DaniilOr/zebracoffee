import numpy as np
import torch
from scipy import spatial

from transformers import AutoTokenizer, AutoModel
tokenizer = AutoTokenizer.from_pretrained("cointegrated/rubert-tiny")
model = AutoModel.from_pretrained("cointegrated/rubert-tiny")


def embed_bert_cls(text, model, tokenizer):
    t = tokenizer(text, padding=True, truncation=True, return_tensors='pt')
    with torch.no_grad():
        model_output = model(**{k: v.to(model.device) for k, v in t.items()})
    embeddings = model_output.last_hidden_state[:, 0, :]
    embeddings = torch.nn.functional.normalize(embeddings)
    return embeddings[0].cpu().numpy()


def get_embeddings(original, analogies):
    orig_embs = np.stack([embed_bert_cls(t, model, tokenizer) for t in original]).mean(0)
    others = []
    for analog in analogies:
        sub = np.stack([embed_bert_cls(t, model, tokenizer) for t in analog]).mean(0)
        others.append(sub)
    others = np.array(others)

    orig_embs = orig_embs[np.newaxis, :]
    return orig_embs, others


def find_closest(orig_embs, others):
    myvals = np.dot(orig_embs, others.T)/(np.linalg.norm(orig_embs)*np.linalg.norm(others))
    max_index = np.argmax(myvals)
    return max_index


if __name__ == "__main__":
    get_embeddings(['Я женя', 'Иииигогор любит сыыыр'], [['одиннадцать двадцать два', 'девять'],
                                                         ['семья это главное', 'дети важны']])